!cat:Index:
!title:Index:
!start:


# LiuPeeker

## Vad är detta?
<p>
LiuPeeker är en hemsida som enkelt låter dig få en översikt av vad som händer på campus via offentligt tillgängliga facebookinlägg.
</p>
## Varför inte bara använda facebook?
<p>
Facebook samlar in enorma mängder personlig data med bristande möjligheter att tacka nej. 
Datan används för att sälja algoritmiskt designad reklam till dig som användare, och lagras under lång tid för att träna allt mer sofistikerade algoritmer.
</p>
<p>
Utöver detta har interna, läckta, rapporter från Facebook visat på att de aktivt destabiliserar demokrati och yttrandefrihet världen över i jakt på profiter. <a href="https://en.wikipedia.org/wiki/2021_Facebook_leak">The Facebook Leaks</a>
</p>
<p>
Om inte annat kan läckan av över <a href="https://edition.cnn.com/2022/07/05/china/china-billion-people-data-leak-intl-hnk/index.html">en miljard kinesiska medborgares polisregister</a> under månadskiftet juni/juli vara en varning om varför man inte bör ge ut för mycket personlig information till stora centraliserade databaser. (Behåll här även i åtanke att Facebook har mycket mer personlig data lagrad än den kinesiska polisen)
</p>
<p>
Vi på LiuPeeker ser alla dessa anledningar som mer än tillräckligt för att ta avstånd till Facebooks plattform. Tyvärr sker mycket av LiUs studentliv på facebook, vilket kan göra det svårt att lämna plattformen utan att isolera sig själv från det sociala. LiuPeeker ger dig nu ett alternativ som låter dig se på sektionernas/föreningarnas facebookinlägg, fritt, gratis, och helt anonymt. 
</p>
## Vilka är ni?
<p>
Vi är en grupp medlemmar från datorföreningen <a href="https://www.lysator.liu.se/">Lysator</a> som tycker om enkla webbsidor som inte spårar dig. 
</p>
## Jag har upptäckt en bugg/vill ge feedback. Var kan jag göra det?
<p>
Hittar du ett problem med hemsidan kan du rapportera det <a href="https://git.lysator.liu.se/liu-aktivitetsvy/face2file/-/issues">här</a> <br />
</p>
## Min förening/sektion finns inte med!
<p>
Ingen fara. Öppna bara en problemrapport <a href="https://git.lysator.liu.se/liu-aktivitetsvy/face2file/-/issues">här</a> och berätta vilken förening/sektion vi har missat så lägger vi till den i vår lista av facebook-sidor som spåras. <br />
</p>

## Vad mer borde jag veta?
<p>
* LiuPeeker har öppen källkod, det betyder att du kan granska koden som körs både på servern och hemsidan (<a href="https://git.lysator.liu.se/liu-aktivitetsvy/face2file">här</a>) och bidra till den om du så vill. <br />
* LiuPeeker är liscensierat under <a href="https://git.lysator.liu.se/liu-aktivitetsvy/face2file/-/blob/master/LICENSE">BSD-3</a> vilket måste hållas i åtanke om du vill bidra med kod eller starta en egen instans (vilket du gärna får). <br />
* För dig som är riktigt paranoid finns även LiuPeeker tillgängligt på TOR via denna länk: [Implementeras, tack för ditt tålamod :)] <br />
* Vill du skriva med oss direkt finns vi på irc.libera.chat under <a href="https://web.libera.chat/?randomnick=1&channels=#liupeeker">#liupeeker</a>
</p>


#!/bin/sh

pagelist=$(cat pagelist.txt.backup | grep -v -e '^$' | grep -v "#" )
for Line in $pagelist
do
	pagehasdoc=$(ls txt | grep -e "^$Line$" | wc -l )
	if [ $pagehasdoc = 0 ]; then
	echo "add page $Line [y/n]"
	read -r yn
	while true
	do
		if [ "$yn" = n ]; then
			break;
		fi
		if [ "$yn" = y ]; then
			mkdir $Line
			echo "Title of page:"
			while true;
			do
				read -r title
				if [ $title != "" ]; then
					break;
				fi
				echo "empty title not allowed!"
				echo "enter title of page with filename $Line"
			done
			subcategories=$(grep "!impsub:" txt/index.md | awk -F":" '{print $2}')
			select ITEM in $subcategories
			do
				echo "!sub:$ITEM:" > "txt/$Line.md"
				echo "!title:$title:" >> "txt/$Line.md"
				printf "!start:\n\n# $title:\n\n" >> "txt/$Line.md"
				echo "!impblog:$Line:" >> "txt/$Line.md"
				break
			done
			break
		fi
		echo "please answer y/n - small letters"
	done
	fi
done

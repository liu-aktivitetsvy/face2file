GXX=clang++

all: converter

main.o: main.cpp
	$(GXX) -std=c++17 -g -c main.cpp

converter: main.o
	$(GXX) -std=c++17 -g main.o -o converter

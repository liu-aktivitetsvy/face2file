#!/bin/sh

while true
do
	pagelist=$(cat pagelist.txt.backup | grep -v -e '^$' | grep -v "#" )
	for Line in $pagelist
	do
		echo "$Line"
		cat pagelist.txt.template > pagelist.txt
		echo "$Line" >> pagelist.txt
		python3 Seleniumparser.py
		sleep 2
	done
	~/cshg/cshg -i -d txt/
	cp -r html/* ~/.liupeeker/html/
#	randomwaittime=$(echo $(( $RANDOM % 720 + 420 )))
#	echo "waiting for $randomwaittime"
#	sleep $randomwaittime
done

#!/bin/bash

echo "Title of page:"
read -r title

pageurls=$(cat page)
echo "Filename of page:"
read -r filename

echo "Folder with page entries:"
read -r folder

subcategories=$(grep "!impsub:" txt/index.md | awk -F":" '{print $2}')
select ITEM in $subcategories
do
	echo "!sub:$ITEM:" > "txt/$filename.md"
	echo "!title:$title:" >> "txt/$filename.md"
	printf "!start:\n\n# $title:\n\n" >> "txt/$filename.md"
	echo "!impblog:$folder:" >> "txt/$filename.md"
	break
done

#include <string>
#include <iostream>
#include <filesystem>
#include <fstream>
#include <regex>
#include <vector>

using namespace std;

int main(int argc, char** argv)
{
//-------------	Open file from arg and initiate filestreams
//	if( argc < 2 )
//	{
//		cout << "No arguments... \n Exiting" << endl;
//		return 0;
//	}
	//system("python3 Seleniumparser.py");//refresh data


	ifstream pagelist_file;
	pagelist_file.open("pagelist.txt");//open list of webpages to process

	string pagelist_string;
	string foldername[512];
	int pagecount = 0;
	while(getline(pagelist_file, pagelist_string))
	{
		size_t commentpos = pagelist_string.find("#");
		if( commentpos != string::npos )
		{
			pagelist_string.erase(commentpos, pagelist_string.size()-commentpos); //erase line after #
		}
		if( pagelist_string.size() > 0 && pagecount < 512 )
		{
			foldername[pagecount] = pagelist_string; //line to foldername
			pagecount++;
		}
		if( pagecount >= 512 )
		{
			cout << "too many pages scanned, array overflow (512), page " << pagelist_string << "skipped" << endl;
		}
	}

	ifstream i;
	ofstream o; //input output

	for(int filnummer = 0; filnummer <= pagecount; filnummer++ )
	{
		string importfile = foldername[filnummer];
		importfile.append(".txt");
		i.open(importfile);
		
		string destinationfolder = foldername[filnummer];
		destinationfolder.append("/");
		cout << destinationfolder << endl;
		
        //-------------	One line = one post
		string line;
		string Content;
		string Date;
		while(getline(i, line))
		{
			int cleartogo = 1;
			char rawdateofpost[30];
			while(cleartogo >= 1)
			{
			//-------------	Extract date from post
				size_t abbrpos = line.find("<abbr>");
				if( abbrpos != string::npos )
				{
					//*debug*/cout << abbrpos << "\t";
					int iterator = (abbrpos + 6);
					int n = 0;
					for( int b = 0; b < 30; b++ )
					{
						rawdateofpost[b] = '\0';
					}
					while( line[iterator] != '<' && n < 30)
					{
						rawdateofpost[n] = line[iterator];
						n++;
						iterator++;
					}
					for( int a = 0; a < n; a++ )
					{
						cout << rawdateofpost[a];
					}
					cout << "\n\n";
			//		char Month[10];
			//		for(int a = 0; a < 10; a++)
			//		{
			//			if( rawdateofpost == ' ' ){a = 10;}
			//			Month[a] = rawdateofpost[a];
			//		}
		
			//		Day.erase(rawdateofpost.find(" "), rawdateofpost.size()-rawdateofpost.find(" "));
			//		cout << Day << endl;
		
				}
			//------------- Extract contents from post
				size_t scansize_t = line.find("<u>More options</u>");
				int scanpos = static_cast<int>(scansize_t);
				scanpos += 19;
				if( scansize_t != string::npos )
				{
					regex spanclose ("\\</span\\>");
					regex paragraphclose ("\\</p\\>");
					regex htmltags ("\\<.*?\\>");
					Content.assign(line, scanpos, line.size()-scanpos );
					Content = regex_replace(Content, spanclose, "\n" );
					Content = regex_replace(Content, paragraphclose, "  \n" );
					Content = regex_replace(Content, htmltags, "" );
					string fullfilename = destinationfolder;
					fullfilename.append(rawdateofpost);
					o.open(fullfilename);
					o << "## " << Content << "\n\n" << endl;
					o.close();
				}
		
			//------------- Extract image link from post
				vector<string> postlinks = {};
				size_t scanimage_t = line.find("<img src=\"");
				if( scanimage_t != string::npos)
				{
					regex link("<img src=\".*?(?=\")");
					std::copy( \
						sregex_token_iterator(line.begin(), line.end(), link), \
						sregex_token_iterator(), std::back_inserter(postlinks));
					
					for(string linked : postlinks)
					{
						linked.erase(0, linked.find("<img src=\"")+10);
						string command = "wget " + linked;
						cout << command.c_str() << endl;
					}
				}
				cleartogo--;
			}
		}
		i.close();
	}
	return 0;
}

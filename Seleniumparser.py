from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.action_chains import ActionBuilder
from selenium.webdriver.support import expected_conditions as EC
from PIL import Image
from pathlib import Path
import urllib
import PIL
import requests
import re
import os
from datetime import timedelta
import datetime
import time
import magic
import hashlib

#make browser run in background
options = Options()
options.headless = True

def configparser():
    #handling page list
    urllist = []
    domain = "https://facebook.com/"
    with open("pagelist.txt", "r") as pagelist:
        for i in pagelist:
            if i.rstrip():    
                if "#" in i:
                    page = re.findall(".*?(?=#)", i)
                    if page[0] == '':
                        pass
                    else:
                        page = page[0].replace(" ", "")
                        urllist.append(domain+page)
                else:
                    page = i.replace("\n", "")
                    urllist.append(domain+page)
    return urllist

def scraper(urllist):
    #scraper
    with webdriver.Firefox(options=options) as driver:
        
        pagebunch = []
        #fetch the website at an appropriate size, to standardise scrolling amounts later
        driver.set_window_size(1920, 1080)
        urlnum=0
        for url in urllist:
            urlnum+=1
            driver.get(url)
            time.sleep(1)
            print(urlnum, url)
            pageName = re.sub(".*/","", url)
            
            #bypass cookies
            if url == urllist[0]:
                try:
                    WebDriverWait(driver=driver, timeout=5).until( 
                        EC.presence_of_element_located((By.ID, 'm_login_email')))
                    driver.find_element(by=By.CSS_SELECTOR, value='m_login_email').send_keys("liupeeker@protonmail.com")
                    driver.find_element(by=By.CSS_SELECTOR, value='m_login_password').send_keys("")
                except:
                    pass

                try:
                    WebDriverWait(driver=driver, timeout=15).until( 
                        EC.presence_of_element_located((By.CSS_SELECTOR, '[role="button"]')))
                    driver.find_element(by=By.XPATH, value='//span[.="Only allow essential cookies"]').click()
                except:
                    try:
                        WebDriverWait(driver=driver, timeout=15).until( 
                        EC.presence_of_element_located((By.CSS_SELECTOR, 'data-cookiebanner="accept_only_essential_button"')))
                        driver.find_element(by=By.XPATH, value='data-cookiebanner="accept_only_essential_button"').click()
                    except:
                        pass
           
           #trigger and wait for images to load in
            try:
                WebDriverWait(driver=driver, timeout=60, poll_frequency = 5).until( 
                    EC.presence_of_element_located((By.CSS_SELECTOR, '[role="article"]')))
                driver.execute_script("window.scrollTo(0, 2500)") 
                time.sleep(5)
                WebDriverWait(driver=driver, timeout=60).until( 
                    EC.presence_of_element_located((By.CSS_SELECTOR, '[role="article"]')))
                CommentSections = driver.find_elements(by=By.PARTIAL_LINK_TEXT, value=" Comment")
            except:
                print("page failed to load")
                continue
            for Comments in CommentSections:
                try:
                     ActionChains(driver).move_to_element(Comments).perform()
                     Comments.click()
                     time.sleep(2)
                except:
                    pass
            postCount = 0    
            while postCount < 5:
                textExpansion = driver.find_elements(by=By.XPATH, value='//div[.="See more"]')
                seeMoreCount = len(textExpansion)
                if seeMoreCount == 0:
                    break
                postCount += seeMoreCount
                for texts in textExpansion:
                    try:
                        desiredY = texts.location['y']-200
                        driver.execute_script(f"window.scrollTo(0, {desiredY})") 
                        texts.click()
                        driver.execute_script(f"window.scrollTo(0, -{desiredY})") 
                        time.sleep(2)
                    except:
                        pass

            session_box = driver.find_elements(by=By.CSS_SELECTOR, value='[role="article"]')
            if len(session_box) < 5:
                session_box = session_box[0:len(session_box)]
            else:
                session_box = session_box[0:5]
            #fetch image links
            postbunch = [] 
            filterStr = ["static", "data:image/svg+xml"]
            for result in session_box:
                if result.get_attribute("aria-label"):
                    if "Comment" in result.get_attribute("aria-label") or "Reply" in result.get_attribute("aria-label"):
                        continue
                else:
                    Date = result.find_elements(by=By.TAG_NAME, value="a")
                    if Date:
                        pageLinks = {}
                        for element in Date:
                            if element.get_attribute("href"):
                                pageLinks[element.text] = element.get_attribute("href")
                        
                        Images = result.find_elements(by=By.TAG_NAME, value="img")
                        try:
                            pfp = result.find_element(by=By.TAG_NAME, value="image")
                            Images.insert(0, pfp)
                        except:
                            print("no 'image' tags, use only img and move on")
                            pass
                        for D in Date:
                            T = D.text
                            N = D.get_attribute("aria-label")
                            if N == T:
                                Date = N
                                break
                        if type(Date) is list:
                            try:
                                Date = result.find_element(by=By.XPATH, value="//abbr/span").text
                            except:
                                print("missing date error, skipping entry")
                                continue
                        Content = result.text 
                        Content = re.sub(Date, "", Content)
                        try:
                            CoarseDate = datetime.datetime.strptime(Date, "%B %d at %I:%M %p").replace(year=datetime.date.today().year).strftime("%Y-%m-%d-%H:%M")
                        except:
                            if "Just now" in Date:
                                CoarseDate = datetime.datetime.now().strftime("%Y-%m-%d-%H:%M")
                            elif "mins" in Date:
                                MinShift = int(re.sub(" mins", "", Date))
                                PostHour = datetime.datetime.now() - timedelta(hours=0, minutes=MinShift)
                                CoarseDate = PostHour.replace(year=datetime.date.today().year).strftime("%Y-%m-%d-%H:%M")
                            elif "hr" in Date:
                                HourShift = int(re.sub(" hr.*", "", Date))
                                PostHour = datetime.datetime.now() - timedelta(hours=HourShift, minutes=0)
                                CoarseDate = PostHour.replace(year=datetime.date.today().year).strftime("%Y-%m-%d-%H:%M")
                            elif "Today" in Date:
                                CoarseDate = datetime.datetime.strptime(Date, "Today at %I:%M %p")
                                dateday = datetime.date.today().day
                                datemonth = datetime.date.today().month
                                dateyear = datetime.date.today().year
                                CoarseDate = CoarseDate.replace(day=dateday, month=datemonth, year=dateyear).strftime("%Y-%m-%d-%H:%M")
                            elif "Yesterday" in Date:
                                CoarseDate = datetime.datetime.strptime(Date, "Yesterday at %I:%M %p")
                                dateday = datetime.date.today().day - 1
                                if dateday <= 0:
                                    datemonth = datetime.date.today().month - 1
                                    if datemonth <= 0:
                                        dateyear = datetime.date.today().year - 1
                                else:
                                    datemonth = datetime.date.today().month
                                    dateyear = datetime.date.today().year
                                CoarseDate = CoarseDate.replace(day=dateday, month=datemonth, year=dateyear).strftime("%Y-%m-%d-%H:%M")
                            else:
                                try:
                                    if datetime.datetime.strptime(Date, "%B %d, %Y"):
                                        CoarseDate = datetime.datetime.strptime(Date, "%B %d, %Y").strftime("%Y-%m-%d")
                                except:
                                    if datetime.datetime.strptime(Date, "%B %d"):
                                        CoarseDate = datetime.datetime.strptime(Date, "%B %d").replace(year=datetime.date.today().year).strftime("%Y-%m-%d")
                        try:
                            SoftDate = datetime.datetime.strptime(CoarseDate, "%Y-%m-%d-%H:%M").strftime("%d %B %Y at %H:%M")
                        except:
                            SoftDate = datetime.datetime.strptime(CoarseDate, "%Y-%m-%d").strftime("%B %d, %Y")
                        pageName = url.rsplit("/", 1)[-1]
                        CoarseDate += f"-{pageName}"
                        rawDate = Date
                        print(SoftDate, CoarseDate)
                        imageLinks = []
                        #print(Content, "\n--------------------------")
                        for element in Images:
                            link = element.get_property('src')
                            if link is None:
                                link = element.get_property('href')["baseVal"]
                            if not any(filters in link for filters in filterStr):
                                imageLinks.append(link)
                        postbunch.append([[CoarseDate, SoftDate, rawDate], Content, imageLinks, pageLinks])   

            pagebunch.append([pageName, postbunch])
             
    driver.quit()
    return pagebunch

def postformatter(pagebunch):

    for page in pagebunch:
        for post in page[1]: 
            
            directoryPath = Path(page[0])
            filePath = directoryPath / post[0][0]
            try:
                os.mkdir(directoryPath)
            except:
                pass

            ImagePaths = []
            trustedImagePaths = []
            ImageCount = 0
            for link in post[2]:
                ImagePath = Path("images") / f"{post[0][0]}{ImageCount}"
                with open(f"html/{ImagePath}", 'wb') as file:
                    file.write(requests.get(link, stream=True).content)
                    file.close()
                ext = magic.from_buffer(open(f"html/{ImagePath}", 'rb').read(2048))
                ext = ext.split(" ", 1)[0].lower()
                if ext == "jpeg":
                    ext = "jpg"
                CheckedImagePath = f"{ImagePath}.{ext}"
                if CheckedImagePath.rsplit("/", 1)[-1] in os.listdir("html/images"):
                    trustedImagePaths.append(CheckedImagePath)
                    ImageCount+=1
                    os.remove(f"html/{ImagePath}")
                    #print(f"{CheckedImagePath} exists, removing {ImagePath}") 
                else:
                    os.rename(f"html/{ImagePath}", f"html/{CheckedImagePath}")
                    ImagePaths.append(CheckedImagePath)
                    ImageCount+=1
                    #print(CheckedImagePath)
 

            TitleDate = post[0][1]
            postString = ""
            postString += post[1]
            
            postHasher = hashlib.sha256(b'')
            heuristicHashMatch = post[3][post[0][2]].split("?", 1)[0]
            print(heuristicHashMatch)
            heuristicHashMatch = heuristicHashMatch.encode('utf-8')
            hashedPost = hashlib.sha256(heuristicHashMatch).hexdigest()
            print(hashedPost)
            
            for text, link in post[3].items():
                parsedLink = urllib.parse.urlparse(link)
                if "u=https%3A%2F%2F" in parsedLink.query:
                    parsedLink = urllib.parse.unquote(parsedLink.query)
                    parsedLink = parsedLink.split("=", 1)[-1]
                    link = parsedLink.split("&h=", 1)[0]
                Etext = re.escape(text)
                if text != '' and link != '':
                    #markDownText = f"[{text}]({link})"
                    markDownText = f'<a href="{link}">{text}</a>'
                    postString = re.sub(Etext, markDownText, postString)
 
            if ImagePaths:
                for i in ImagePaths[1:]:
                    postString += f"  \n![Image]({i})\n"
            else:
                for i in trustedImagePaths[1:]:
                    postString += f"  \n![Image]({i})\n"
            postString = re.sub("(?<=.)\n(?=.)", "<br />", postString)
            
            postHistory = open("posthistoryhash.txt", "r")
            downloadedImageCount = len([imageName for imageName in os.listdir("html/images") if post[0][0] in imageName])
            if hashedPost in postHistory.read():
                postHistory.close()
                for image in ImagePaths:
                    os.remove(f"html/{image}")
                print("duplicate")
            elif len(ImagePaths) >= downloadedImageCount or len(trustedImagePaths) >= downloadedImageCount:
                postHistory.close()
            
                with open("posthistoryhash.txt", "a") as postHistory:
                     postHistory.write(f"\n{hashedPost}")
                     postHistory.close()
                
                try:
                    postString = f"![Profile Picture]({ImagePaths[0]}) ## {TitleDate}  \n {postString}"
                except:
                    print("ImagePaths empty")
                    postString = f"![Profile Picture]({trustedImagePaths[0]}) ## {TitleDate}  \n {postString}"
                    pass

                
                with open(filePath, 'w') as postfile:
                    postfile.write(postString)
                    postfile.close()

def main():
    urllist = configparser()
    pagebunch = scraper(urllist)
    postformatter(pagebunch)
    return 0

if __name__ == "__main__":
    main()
